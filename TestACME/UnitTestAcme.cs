using ACME.Business;
using ACME.Domain;
using NUnit.Framework;
using System.Collections.Generic;

namespace TestACME
{
    public class Tests
    {
        string FileInput = @"C:\ACME\INPUT.txt";

        /// <summary>
        /// Evaluate quantity of register that getListEmployee return
        /// </summary>
        [Test]
        public void TestGetListEmployee()
        {
            
            List<EmployeeDTO> lstEmployee;
            EmployeeBusiness employeeBusiness = new EmployeeBusiness();

            lstEmployee = employeeBusiness.GetListEmployee(FileInput);
            Assert.AreEqual(3, lstEmployee.Count);
        }

        /// <summary>
        /// Evaluate quantity of register that GetSameTimeFrame return
        /// </summary>
        [Test]
        public void TestGetSameTimeEmployee()
        {
            List<EmployeeDTO> lstEmployee;
            List<string> lstResult;
            EmployeeBusiness employeeBusiness = new EmployeeBusiness();

            lstEmployee = employeeBusiness.GetListEmployee(FileInput);
            lstResult = employeeBusiness.GetSameTimeFrame(lstEmployee);

            Assert.AreEqual(3, lstResult.Count);
        }

        /// <summary>
        /// Evaluate if the process return a specific  value
        /// </summary>
        [Test]
        public void TestGetSameTimeEmployeespecific()
        {
            List<EmployeeDTO> lstEmployee;
            List<string> lstResult;
            EmployeeBusiness employeeBusiness = new EmployeeBusiness();

            lstEmployee = employeeBusiness.GetListEmployee(FileInput);
            lstResult = employeeBusiness.GetSameTimeFrame(lstEmployee);

            Assert.IsTrue(lstResult.Exists(p => p.Contains("RENE-ASTRID: 2")));

        }

    }
}