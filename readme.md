# ACME

The goal of this exercise is to output a table containing pairs of employees and how often they have coincided in the office.

## Getting started

This project resolve a problem for ACME who offers their employees the flexibility to work the hours they want. But due to some external circumstances they need to know what employees have been at the office within the same time frame.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/carlosleonardo1/ACME.git
git branch -M main
git push -uf origin main
```

## Name
ACME Project 

## Description
ACME Project es una aplicación monolítica, lo que quiere decir que es completamente independiente, en términos de su comportamiento y funcionalidad.  La aplicación está contenida en un solo proyecto, se compila en un único ensamblado y se implementa como una sola unidad como una aplicacion de consola. 
Si bien es cierto ACME Project es una aplicación monolítica, se implemento en una arquitectura de n-capas, para poder ayudar a dividir la aplicación segun sus responsabilidades o intereses, lo cual nos ayuda a mantener el código organizado.
Cabe recalcar que debido a que ACME Project es un proyecto de consola cuya funcionalidad es puntual, no se implemento n-capas con proyectos relacionados lo cual nos permite mayores ventajas de la arquitectura y en lugar de ello se implemento en carpetas para separar la logica de la solución. 

ACME Project esta dividio en tres capas:
Presentacion: La cual recibe la informacion del archivo a procesar e invoca a las otras capas para que procesen el archivo y luego se encarga de presentar los resultados.
Business: Se encarga de toda la logica del negocio en nuestro proyecto se encarga de procesar el archivo con los horarios de cada empleado, para ello implementa dos metodos GetListEmployee y GetSameTimeFrame. 
Domain: Contiene las clases conocidas como Data Transfer Objects (DTO), los cuales nos permiten transportar información entre cada una de las capas, en esta capa tenemos dos clases EmployeeDTO y TimeFrameDTO.

La solución tambien contiene un proyecto tipo NUnit Test Project en .NET 5, el cual contiene el desarrollo de un test con tres métodos TestGetListEmployee, TestGetSameTimeEmployee y TestGetSameTimeEmployeespecific.
Estos tres metodos evaluan el correcto funcionamiento de la solución.

## Visuals
![image.png](./image.png)

## Installation
La solución esta desarrollado en NET 5 Framework, con el lenguage de programacion C#, la cual contiene dos proyectos, uno con la solución del problema y otro proyecto de test, fue desarrollado utilizando el IDE visual studio 2019.
Y para abrir el proyecto se debe abrir el archivo principal de la solucion ACME.sln

## Usage
Para su uso se debe crear una carpeta en la ruta C:\ACME, con los siguientes archivos:
* INPUT.txt
* INPUT_TEST.txt
* INPUT2.txt
Los cuales esta adjuntos en la solución.


## Support
Para soporte o dudas comunicarse con Carlos Choez Alvarez. e-mail: carlosleonardo1@gmail.com


## Authors and acknowledgment
Software Engineer Carlos Choez Alvarez 

## License
It's a open source projects.


