﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACME
{
    public class TimeFrameDTO
    {
        public string DAY { get; set; }
        public TimeSpan TIME_FROM { get; set; }
        public TimeSpan TIME_TO { get; set; }
    }
}
