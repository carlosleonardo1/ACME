﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACME.Domain
{
    public class EmployeeDTO
    {
        public int ID_EMPLOYEE { get; set; }
        public string NAME_EMPLOYEE { get; set; }
        //time frame
        public List<TimeFrameDTO> LST_TIME_FRAME { get; set; }
    }
}
