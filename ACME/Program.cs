﻿using ACME.Business;
using ACME.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ACME
{
    /// <summary>
    ///  This a program to a table containing pairs of employees and how often they have coincided in the office.
    /// </summary>
    class Program
    {

        /// <summary>
        /// Main Method
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //Variable declaration
            var FileInput = @"C:\ACME\INPUT_TEST.txt";
            List<EmployeeDTO> lstEmployee;
            List<string> lstResult;
            EmployeeBusiness employeeBusiness = new EmployeeBusiness();

            /*
            This is a metod to create employee objects with his respective time frames.
            Each employee object has a list of time frame objects.
            */
            lstEmployee = employeeBusiness.GetListEmployee(FileInput);

            /*
               This is a metod process the list to get the result.
            */
            lstResult = employeeBusiness.GetSameTimeFrame(lstEmployee);

            //Show the result in console
            foreach (var objResult in lstResult)
            {
                Console.WriteLine(objResult);
            }

        }
    }
}
