﻿using ACME.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACME.Business
{
    public class EmployeeBusiness
    {
        /// <summary>
        ///     This is a metod to create employee objects with his respective time frames.
        ///     Each employee object has a list of time frame objects.
        ///     
        ///     Sample data file:
        ///     RENE=MO10:00-12:00,TU10:00-12:00,TH01:00-03:00,SA14:00-18:00,SU20:00- 21:00
        ///     ASTRID=MO10:00-12:00,TH12:00-14:00,SU20:00-21:00
        ///     ANDRES=MO10:00-12:00,TH12:00-14:00,SU20:00-21:00
        /// </summary>
        /// <param name="FileInput"></param>
        /// <returns>List of Employees with his respective time frames list.</returns>
        public List<EmployeeDTO> GetListEmployee(string FileInput)
        {
            //Variable declaration
            List<EmployeeDTO> lstEmployee = new List<EmployeeDTO>();
            List<TimeFrameDTO> lstTimeFrame;
            EmployeeDTO objEmployee;
            TimeFrameDTO objTimeFrame;

            //Read lines from File
            string[] lines = File.ReadAllLines(FileInput);

            var countIndex = 0;
            foreach (var objLine in lines)
            {
                countIndex++;

                lstTimeFrame = new List<TimeFrameDTO>();
                objEmployee = new EmployeeDTO();
                objTimeFrame = new TimeFrameDTO();

                var idxFound = objLine.IndexOf("=");
                //Get name
                var objName = objLine.Substring(0, idxFound);
                var objSchedules = objLine.Substring(idxFound + 1);

                //split each schedule
                string[] objSchedule = objSchedules.Split(',');
                //MO10:00-12:00
                foreach (var sub in objSchedule)
                {
                    objTimeFrame = new TimeFrameDTO();
                    var idxHour = sub.IndexOf("-");
                    var objDay = sub.Substring(0, 2);
                    var objHourFrom = sub.Substring(2, idxHour - 2);
                    var objHourTo = sub.Substring(idxHour + 1);

                    //Save data in object
                    objTimeFrame.DAY = objDay;
                    objTimeFrame.TIME_FROM = TimeSpan.Parse(objHourFrom);
                    objTimeFrame.TIME_TO = TimeSpan.Parse(objHourTo);

                    //Save object in list
                    lstTimeFrame.Add(objTimeFrame);

                }
                //Create object employee
                objEmployee.ID_EMPLOYEE = countIndex;
                objEmployee.NAME_EMPLOYEE = objName;
                objEmployee.LST_TIME_FRAME = lstTimeFrame;

                lstEmployee.Add(objEmployee);
            }

            //Return a list of Employees
            return lstEmployee;
        }


        /// <summary>
        /// This is a metod process the list to get the result.
        /// </summary>
        /// <param name="ListEmployee">List of Employee</param>
        /// <returns>Return a list containing pairs of employees </returns>
        public List<string> GetSameTimeFrame(List<EmployeeDTO> ListEmployee)
        {
            List<string> ListToReturn = new List<string>();
            
            foreach (var objEmployee in ListEmployee)
            {
                //Get list from other employee
                var lstOther = ListEmployee.Where(p => p.ID_EMPLOYEE > objEmployee.ID_EMPLOYEE).ToList();
               
                //var objTupleName = objEmployee.NAME_EMPLOYEE;

                foreach (var objEmployeeOther in lstOther)
                {
                    var objTupleName = objEmployee.NAME_EMPLOYEE + "-" + objEmployeeOther.NAME_EMPLOYEE;
                    var objCountGeneral = 0;
                    //For each list from other employee Time Frame 
                    foreach (var objTimeFrame in objEmployeeOther.LST_TIME_FRAME)
                    {
                        //p:1
                        //objTimeFrame:2
                        //From1 <= To2 and To1 >= From2
                        //Obtengo los dias coincidentes
                        var objCountCoincidence = objEmployee.LST_TIME_FRAME.Count(p => p.DAY == objTimeFrame.DAY
                        && p.TIME_FROM <= objTimeFrame.TIME_TO && p.TIME_TO >= objTimeFrame.TIME_FROM);

                        objCountGeneral = objCountGeneral + objCountCoincidence;
                    }

                    ListToReturn.Add($"{objTupleName}: {objCountGeneral}");
                    //ListToReturn.Add(String.Format("|{0,15}|{1,15}|", objTupleName, objCountGeneral));



                

                }

            }

            return ListToReturn;
        }
    }
}
